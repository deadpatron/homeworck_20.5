// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SnakeGame/Poison.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePoison() {}
// Cross Module References
	SNAKEGAME_API UClass* Z_Construct_UClass_APoison_NoRegister();
	SNAKEGAME_API UClass* Z_Construct_UClass_APoison();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_SnakeGame();
	SNAKEGAME_API UClass* Z_Construct_UClass_UInteractable_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(APoison::execSetNewPoisonLocation)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetNewPoisonLocation();
		P_NATIVE_END;
	}
	void APoison::StaticRegisterNativesAPoison()
	{
		UClass* Class = APoison::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "SetNewPoisonLocation", &APoison::execSetNewPoisonLocation },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_APoison_SetNewPoisonLocation_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APoison_SetNewPoisonLocation_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Poison.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APoison_SetNewPoisonLocation_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APoison, nullptr, "SetNewPoisonLocation", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APoison_SetNewPoisonLocation_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_APoison_SetNewPoisonLocation_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APoison_SetNewPoisonLocation()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APoison_SetNewPoisonLocation_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_APoison_NoRegister()
	{
		return APoison::StaticClass();
	}
	struct Z_Construct_UClass_APoison_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APoison_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_SnakeGame,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_APoison_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_APoison_SetNewPoisonLocation, "SetNewPoisonLocation" }, // 526799704
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APoison_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Poison.h" },
		{ "ModuleRelativePath", "Poison.h" },
	};
#endif
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_APoison_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UInteractable_NoRegister, (int32)VTABLE_OFFSET(APoison, IInteractable), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_APoison_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APoison>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APoison_Statics::ClassParams = {
		&APoison::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_APoison_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_APoison_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APoison()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APoison_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APoison, 418476046);
	template<> SNAKEGAME_API UClass* StaticClass<APoison>()
	{
		return APoison::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APoison(Z_Construct_UClass_APoison, &APoison::StaticClass, TEXT("/Script/SnakeGame"), TEXT("APoison"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APoison);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
