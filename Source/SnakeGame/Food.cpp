// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include <cstdlib>
#include <ctime>


// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshFoodComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshFoodComponent"));
	MeshFoodComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshFoodComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
	

}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	SetNewFoodLocation();
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake) && Snake->MovementSpeed > 0.1)
		{
			Snake->AddSnakeElement(1);
			Snake->MovementSpeed -= 0.05;
			Snake->SetActorTickInterval(Snake->MovementSpeed);
			SetNewFoodLocation();
		}
	}
}

void AFood::SetNewFoodLocation()
{
	srand(time(NULL));
	int x = -400 + rand() % 800;
	int y = -400 + rand() % 800;
	int z = 50;
	FVector NewFoodLocation(x, y, z);
	this->SetActorLocation(NewFoodLocation);
}

