// Fill out your copyright notice in the Description page of Project Settings.


#include "Poison.h"
#include "SnakeBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include <cstdlib>
#include <ctime>


// Sets default values
APoison::APoison()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void APoison::BeginPlay()
{
	Super::BeginPlay();
	SetNewPoisonLocation();
	
}

// Called every frame
void APoison::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APoison::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->DeleteSnakeElement();
			Snake->MovementSpeed += 0.05;
			Snake->SetActorTickInterval(Snake->MovementSpeed);
			SetNewPoisonLocation();
		}
	}
}

void APoison::SetNewPoisonLocation()
{
	srand(time(NULL));
	srand(rand());
	int x = -400 + rand() % 800;
	int y = -400 + rand() % 800;
	int z = 50;
	FVector NewFoodLocation(x, y, z);
	this->SetActorLocation(NewFoodLocation);
}

